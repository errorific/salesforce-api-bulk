#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Salesforce::API::Bulk' ) || print "Bail out!\n";
}

diag( "Testing Salesforce::API::Bulk $Salesforce::API::Bulk::VERSION, Perl $], $^X" );
