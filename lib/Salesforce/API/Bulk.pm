package Salesforce::API::Bulk;

=head1 NAME

Salesforce::API::Bulk - The great new Salesforce::API::Bulk!

=head1 VERSION

Version 0.01

=cut

use 5.006;
use strict;
use warnings;
use MooseX::Declare;
use Method::Signatures::Modifiers;

class Salesforce::API::Bulk {
  our $VERSION = '0.01';
  $VERSION = eval $VERSION;

  use Salesforce::API::Bulk::Client;
  use Salesforce::API::Bulk::Job;
  use MooseX::Types::URI;

  has [qw/user pass/] => (
    is => 'ro',
    isa => 'Str',
    required => 1,
  );

  has 'login_uri' => (
    is => 'ro',
    isa => 'MooseX::Types::URI::Uri',
    default => 'https://login.salesforce.com/services/Soap/u/24.0',
    required => 1,
    coerce => 1,
  );

  has '_client' => (
    is => 'ro',
    isa => 'Salesforce::API::Bulk::Client',
    lazy_build => 1,
  );

  method _build__client {
    my $client = Salesforce::API::Bulk::Client->new(
      user => $self->user,
      pass => $self->pass,
      login_uri => $self->login_uri,
    );

    return $client;
  }

  method job ($object) {
    return Salesforce::API::Bulk::Job->new(
      client => $self->_client,
      object => $object,
      operation => 'query',
    );
  }

}

__END__

=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Salesforce::API::Bulk;

    my $foo = Salesforce::API::Bulk->new();
    ...


=head1 AUTHOR

Christopher Mckay, C<< <cmckay at iseek.com.au> >>

=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2012 Christopher Mckay.

This program is released under the following license: restrictive
