use MooseX::Declare;
use Method::Signatures::Modifiers;

class Salesforce::API::Bulk::Client {
  our $VERSION = '0.01';
  $VERSION = eval $VERSION;

  use REST::Client;
  use MooseX::Types::URI;
  use XML::Simple;

  has [qw/user pass/] => (
    is => 'ro',
    isa => 'Str',
    required => 1,
  );

  has 'login_uri' => (
    is => 'ro',
    isa => 'MooseX::Types::URI::Uri',
    required => 1,
    coerce => 1,
  );

  has 'parser' => (
    is => 'ro',
    isa => 'XML::Simple',
    lazy_build => 1,
  );

  method _build_parser {
    return XML::Simple->new;
  }

  has 'session_id' => (
    is => 'ro',
    isa => 'Str',
    writer => '_set_session_id',
    lazy_build => 1,
  );

  method _build_session_id {
    die 'You must provoke a connection before a session id becomes available.';
  }

  has '_client' => (
    is => 'ro',
    isa => 'REST::Client',
    lazy_build => 1,
    handles => [qw/GET PUT POST DELETE getUseragent getHost/],
  );

  method _build__client {
    my $client = REST::Client->new();

    my $login_message = <<SOAP;
<?xml version="1.0" encoding="utf-8" ?>
<env:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
  <env:Body>
    <n1:login xmlns:n1="urn:partner.soap.sforce.com">
      <n1:username>${\($self->{user})}</n1:username>
      <n1:password>${\($self->{pass})}</n1:password>
    </n1:login>
  </env:Body>
</env:Envelope>
SOAP

    my $res = $client->POST($self->login_uri, $login_message, {
        'Content-Type' => 'text/xml',
        'SOAPAction' => 'login',
      }
    );

    my $xml_res = $res->responseContent;
    my $sid;
    my $serverurl;

    if ($xml_res =~ m/<sessionId>(.+)<\/sessionId>/) {
      $sid = $1;
    } else {
      die "No session id provided in response";
    }

    if ($xml_res =~ m/<serverUrl>(.+)<\/serverUrl>/) {
      $serverurl = $1;
    } else {
      die "No server url provided in response";
    }

    $self->_set_session_id($sid);
    $client->addHeader('X-SFDC-Session' => $sid);
    #$client->setHost($serverurl);
    $client->setHost('https://na4-api.salesforce.com');

    return $client;
  }
}
