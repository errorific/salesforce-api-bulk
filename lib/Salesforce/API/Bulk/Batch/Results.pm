use MooseX::Declare;
use Method::Signatures::Modifiers;

class Salesforce::API::Bulk::Batch::Results {
  our $VERSION = '0.01';
  $VERSION = eval $VERSION;

  use Salesforce::API::Bulk::Client;
  use Salesforce::API::Bulk::Batch;
  use File::Temp qw/ :seekable /;

  has _client => (
    is => 'ro',
    isa => 'Salesforce::API::Bulk::Client',
    required => 1,
    init_arg => 'client',
  );

  has _batch => (
    is => 'ro',
    isa => 'Salesforce::API::Bulk::Batch',
    init_arg => 'batch',
    #weak_ref => 1,
  );

  has _result_ids => (
    traits => ['Array'],
    is => 'ro',
    isa => 'ArrayRef[Str]',
    init_arg => 'result_ids',
    required => 1,
    handles => {
      _result_ids_iterator => ['natatime', 1],
    },
  );

  has _iterator => (
    is => 'ro',
    clearer => '_clear_iterator',
    lazy_build => 1,
  );

  method _build__iterator {
    return $self->_result_ids_iterator;
  }

  method first {
    $self->_clear_iterator;
    my $id = $self->_iterator->();
    if ($id) {
      return $self->_get_result($id);
    } else {
      return ;
    }
  }

  method next {
    my $id = $self->_iterator->();
    if ($id) {
      return $self->_get_result($id);
    } else {
      return ;
    }
  }

  method _get_result ($id) {
    my $ua = $self->_client->getUseragent;

    my $url = $self->_client->getHost
      . '/services/async/24.0/job/' . $self->_batch->_job->id 
      . '/batch/' . $self->_batch->id
      . '/result/' . $id;

    print STDERR "result from $url, sid " . $self->_client->session_id . "\n";
    my $fh = File::Temp->new();

    my $res = $ua->get($url, 
      'X-SFDC-Session' => $self->_client->session_id,
      ':content_file' => $fh->filename);

    unless ($res->is_success) {
      die 'Result retrieval failed, code ' . $res->code
      . ' ' . $res->message
      . ' error ' . $res->content;
    }

    return $fh;
  }
}
