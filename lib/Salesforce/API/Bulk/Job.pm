use MooseX::Declare;
use Method::Signatures::Modifiers;

class Salesforce::API::Bulk::Job {
  our $VERSION = '0.01';
  $VERSION = eval $VERSION;

  use Salesforce::API::Bulk::Client;
  use Salesforce::API::Bulk::Batch;

  has '_client' => (
    is => 'ro',
    isa => 'Salesforce::API::Bulk::Client',
    required => 1,
    init_arg => 'client',
  );

  has 'operation' => (
    is => 'ro',
    isa => 'Str', # query or insert, whatever, type this properly
    required => 1,
  );
  
  has 'object' => (
    is => 'ro',
    isa => 'Str',
    required => 1,
  );

  has 'id' => (
    is => 'ro',
    writer => '_set_id',
    isa => 'Str',
  );

  method BUILD (@args) {
    my $res = $self->_client->POST(
      '/services/async/24.0/job',
      <<XML,
<?xml version="1.0" encoding="UTF-8"?>
<jobInfo xmlns="http://www.force.com/2009/06/asyncapi/dataload">
  <operation>${\($self->operation)}</operation>
  <object>${\($self->object)}</object>
  <concurrencyMode>Parallel</concurrencyMode>
  <contentType>CSV</contentType>
</jobInfo>
XML
      {
        'Content-Type' => 'application/xml',
      }
    );

    unless ($res->responseCode == 201) {
      die 'Job creation failed, code ' . $res->responseCode
      . ' error ' . $res->responseContent;
    }

    my $res_content = $self->_client->parser->XMLin($res->responseContent);

    $self->_set_id($res_content->{id});
  }

  method close {
    my $res = $self->_client->POST(
      '/services/async/24.0/job/' . $self->id,
      <<XML,
<?xml version="1.0" encoding="UTF-8"?>
<jobInfo xmlns="http://www.force.com/2009/06/asyncapi/dataload">
  <state>Closed</state>
</jobInfo>
XML
      {
        'Content-Type' => 'application/xml',
      }
    );

    unless ($res->responseCode == 201) {
      die 'Job closure failed, code ' . $res->responseCode
        . ' error ' . $res->responseContent;
    }

  }

  method batch ($query) {
    return Salesforce::API::Bulk::Batch->new(
      client => $self->_client,
      job => $self,
      query => $query,
    );
  }
}
