use MooseX::Declare;
use Method::Signatures::Modifiers;

class Salesforce::API::Bulk::Batch {
  our $VERSION = '0.01';
  $VERSION = eval $VERSION;

  use Salesforce::API::Bulk::Client;
  use Salesforce::API::Bulk::Job;
  use Salesforce::API::Bulk::Batch::Results;

  has _client => (
    is => 'ro',
    isa => 'Salesforce::API::Bulk::Client',
    required => 1,
    init_arg => 'client',
  );

  has _job => (
    is => 'ro',
    isa => 'Salesforce::API::Bulk::Job',
    init_arg => 'job',
    #weak_ref => 1,
  );

  has query => (
    is => 'ro',
    isa => 'Str',
    required => 1,
  );

  has 'id' => (
    is => 'ro',
    writer => '_set_id',
    isa => 'Str',
  );

  method BUILD (@args) {
    my $res = $self->_client->POST(
      '/services/async/24.0/job/' . $self->_job->id . '/batch',
      $self->query,
      {
        'Content-Type' => 'text/csv',
      }
    );

    unless ($res->responseCode == 201) {
      die 'Batch creation failed, code ' . $res->responseCode
      . ' error ' . $res->responseContent;
    }

    my $res_content = $self->_client->parser->XMLin($res->responseContent);

    $self->_set_id($res_content->{id});
  }

  method state {
    return $self->_query->{state};
  }

  method _query {
    my $res = $self->_client->GET(
      '/services/async/24.0/job/'
        . $self->_job->id . '/batch/' . $self->id,
      {
        'Content-Type' => 'application/xml'
      }
    );

    unless ($res->responseCode == 200) {
      die 'Batch query failed, code ' . $res->responseCode
      . ' error ' . $res->responseContent;
    }

    return $self->_client->parser->XMLin($res->responseContent);
  }

  method results {
    my $state = $self->state;
    unless ($state eq 'Completed') {
      die "Batch in state $state cannot return results until it has completed";
    }

    my $res = $self->_client->GET(
      '/services/async/24.0/job/' . $self->_job->id 
      . '/batch/' . $self->id
      . '/result',
    );

    unless ($res->responseCode == 200) {
      die 'Batch query failed, code ' . $res->responseCode
      . ' error ' . $res->responseContent;
    }

    my $results = $self->_client->parser->XMLin($res->responseContent,
      ForceArray => [qw/result/]);

    return Salesforce::API::Bulk::Batch::Results->new(
      client => $self->_client,
      batch => $self,
      result_ids => $results->{result},
    );
  }
}
